# This file is a template, and might need editing before it works on your project.
FROM golang:1.8-alpine AS builder

WORKDIR /usr/src/app

COPY . .
RUN go-wrapper download
RUN go build -v
#IMfrom local:
#imfrom local again
FROM alpine:3.5

# We'll likely need to add SSL root certificates
RUN apk --no-cache add ca-certificates

WORKDIR /usr/local/bin
#iamfrommaster
#iamagainfrommaster
#iamfromfrommasteragain
COPY --from=builder /usr/src/app/app .
CMD ["./app"]
